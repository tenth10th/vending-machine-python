import pytest

from vending_machine import VendingMachine


def test_coin():
    grams = 1.23
    millimeters = 3.45
    coin = Coin(weight=grams, diameter=millimeters)
    assert coin.weight == grams
    assert coin.diameter == millimeters


@pytest.mark.parametrize("grams, millimeters", [
    (5.0, 21.21),
    (2.268, 17.91),
    (5.670, 24.26)
])
def test_is_coin_valid_true(grams, millimeters):
    coin = Coin(weight=grams, diameter=millimeters)
    assert is_coin_valid(coin)


@pytest.mark.parametrize("grams, millimeters", [
    (5.0, 21.21),
    (2.268, 17.91),
    (5.670, 24.26)
])
def test_is_coin_valid_false(grams, millimeters):
    coin = Coin(weight=grams + 1, diameter=millimeters)
    assert not is_coin_valid(coin)

    coin = Coin(weight=grams, diameter=millimeters + 1)
    assert not is_coin_valid(coin)


def test_vending_machine_instantiation():
    value = 123
    machine = VendingMachine(value=value)
    assert machine.value == value


def test_vending_machine_with_no_money_displays_insert_coin():
    machine = VendingMachine(0)
    assert machine.display() == "INSERT COIN"

# OR


def test_vending_machine_with_no_money_displays_insert_coin_alternate():
    machine = VendingMachine(0)
    assert get_display(machine) == "INSERT COIN"


def test_vending_machine_display_with_money():
    machine = VendingMachine(0.05)
    assert get_display(machine) == "$0.05"
    assert machine.display() == "$0.05"


def test_vending_machine_insert_valid_nickel():
    nickel = Coin(5.0, 21.21)
    machine = VendingMachine(0)

    machine.insert_coin(nickel)
    new_machine = insert_coin(machine, nickel)


# then with an invalid coin, you need to represent a change slot this could be a separate object
# from vending machine.  perhaps vending machine needs to be renamed
