
class VendingMachine(object):
    def __init__(self, value: float):
        self._value = value

    @property
    def value(self):
        return self._value
